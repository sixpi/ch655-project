#ifndef UTILS_H
#define UTILS_H

#include "mol2/nbenergy.h"

static void compute_energy(double *energy, struct mol_atom_group *ag,
                           struct agsetup *ags)
{
    *energy = 0.0;
    vdweng(ag, energy, ags->nblst);
    eleng(ag, 1.0, energy, ags->nblst);
}

#endif /* UTILS_H */
