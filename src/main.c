// Includes
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "mol2/icharmm.h"
#include "mol2/matrix.h"
#include "mol2/nbenergy.h"
#include "mol2/pdb.h"
#include "mol2/transform.h"

#include "gsl/gsl_rng.h"

#include "utils.h"

gsl_rng *rng = NULL;
struct mol_matrix3_list *rotations = NULL;

static void generate_trial_move(const struct mol_matrix3 **rotation, struct mol_vector3 *tv)
{
    size_t ri = gsl_rng_uniform_int(rng, rotations->size);
    *rotation = &rotations->members[ri];

    tv->X = gsl_rng_uniform_pos(rng) / 10.0; // X in (0.0, 0.1)
    tv->Y = gsl_rng_uniform_pos(rng) / 10.0; // X in (0.0, 0.1)
    tv->Z = gsl_rng_uniform_pos(rng) / 10.0; // X in (0.0, 0.1)
}

static void center_ligand(struct mol_vector3 *center,
                          const struct mol_vector3 *coords, size_t natoms)
{
    MOL_VEC_SET_SCALAR(*center, 0.0);
    for (size_t i = 0; i < natoms; ++i) {
        MOL_VEC_ADD(*center, *center, coords[i]);
    }
    MOL_VEC_DIV_SCALAR(*center, *center, natoms);
}

static void vec_trans(struct mol_vector3* coords, size_t len,
                      const struct mol_vector3 *tv, double scale)
{
    struct mol_vector3 scaled = *tv;
    MOL_VEC_MULT_SCALAR(scaled, scaled, scale);
    for (size_t i = 0; i < len; ++i) {
        MOL_VEC_ADD(coords[i], scaled, coords[i]);
    }
}

int main(int argc, char *argv[]) {
    int argi = 1;

    char *system1_pdb = argv[argi++];
    char *system1_psf = argv[argi++];

    char *system2_pdb = argv[argi++];
    char *system2_psf = argv[argi++];

    size_t ligand_start = strtol(argv[argi++], NULL, 10);
    double lambda = strtod(argv[argi++], NULL);
    double one_minus_lambda = 1.0 - lambda;
    double negative_beta = -strtod(argv[argi++], NULL);

    char *rtf_file = argv[argi++];
    char *prm_file = argv[argi++];
    char *rot_file = argv[argi++];

    struct mol_atom_group *system1 = mol_read_pdb(system1_pdb);
    mol_atom_group_read_geometry(system1, system1_psf, prm_file, rtf_file);
    system1->gradients = calloc(system1->natoms, sizeof(struct mol_vector3));
    struct agsetup system1_ags;

    mol_fixed_init(system1);
    mol_fixed_update(system1, 0, NULL); // Make nothing fixed
    init_nblst(system1, &system1_ags);
    update_nblst(system1, &system1_ags);

    struct mol_atom_group *system2 = mol_read_pdb(system2_pdb);
    mol_atom_group_read_geometry(system2, system2_psf, prm_file, rtf_file);
    system2->gradients = calloc(system2->natoms, sizeof(struct mol_vector3));
    struct agsetup system2_ags;

    mol_fixed_init(system2);
    mol_fixed_update(system2, 0, NULL); // Make nothing fixed
    init_nblst(system2, &system2_ags);
    update_nblst(system2, &system2_ags);

    size_t nrounds = 100;
    if (argi < argc) {
        nrounds = strtol(argv[argi++], NULL, 10);
    }
    size_t nsteps = 100;
    if (argi < argc) {
        nsteps = strtol(argv[argi++], NULL, 10);
    }
    size_t equilibrate = 10;
    if (argi < argc) {
        equilibrate = strtol(argv[argi++], NULL, 10);
    }

    rotations = mol_matrix3_list_from_file(rot_file);

    gsl_rng_env_setup();
    rng = gsl_rng_alloc(gsl_rng_default);

    double cummulative_E = 0.0;
    const struct mol_matrix3 *rotation;
    struct mol_vector3 translation;

    double E1_cur;
    double E2_cur;
    double E1_new;
    double E2_new;

    double E_cur;
    double E_new;

    double delta_E;

    size_t ligand1_size = system1->natoms - ligand_start;
    size_t ligand2_size = system2->natoms - ligand_start;

    struct mol_vector3 ligand1_save[ligand1_size];
    struct mol_vector3 ligand2_save[ligand2_size];

    size_t naccept = 0;
    struct mol_vector3 center;
    center_ligand(&center, &system1->coords[ligand_start], ligand1_size);

    for (size_t i = 0; i < equilibrate; ++i) {
        generate_trial_move(&rotation, &translation);

        // Save current ligand positions
        memcpy(ligand1_save, &system1->coords[ligand_start],
               ligand1_size*sizeof(struct mol_vector3));
        memcpy(ligand2_save, &system2->coords[ligand_start],
               ligand2_size*sizeof(struct mol_vector3));

        // Move both systems the same way
        vec_trans(&system1->coords[ligand_start], ligand1_size,
                  &center, -1.0);
        vec_trans(&system2->coords[ligand_start], ligand2_size,
                  &center, -1.0);
        mol_vector3_move_list(&system1->coords[ligand_start], ligand1_size,
                              rotation, &translation);
        mol_vector3_move_list(&system2->coords[ligand_start], ligand2_size,
                              rotation, &translation);
        vec_trans(&system1->coords[ligand_start], ligand1_size,
                  &center, 1.0);
        vec_trans(&system2->coords[ligand_start], ligand2_size,
                  &center, 1.0);

        compute_energy(&E1_new, system1, &system1_ags);
        compute_energy(&E2_new, system2, &system2_ags);

        E_new = lambda*E1_new + one_minus_lambda*E2_new;
        delta_E = E_new - E_cur;

        if (gsl_rng_uniform(rng) < exp(negative_beta*delta_E)) {
            E_cur = E_new;
            E1_cur = E1_new;
            E2_cur = E2_new;
            center_ligand(&center, &system1->coords[ligand_start], ligand1_size);
        } else {
            // Undo transform
            memcpy(&system1->coords[ligand_start], ligand1_save,
                   ligand1_size*sizeof(struct mol_vector3));
            memcpy(&system2->coords[ligand_start], ligand2_save,
                   ligand2_size*sizeof(struct mol_vector3));
        }
    }

    double cummulative_Ediff = 0.0;
    struct mol_vector3 lig_center;
    for (size_t r = 0; r < nrounds; r++) {
        for (size_t i = 0; i < nsteps; ++i) {
            generate_trial_move(&rotation, &translation);

            // Save current ligand positions
            memcpy(ligand1_save, &system1->coords[ligand_start],
                   ligand1_size*sizeof(struct mol_vector3));
            memcpy(ligand2_save, &system2->coords[ligand_start],
                   ligand2_size*sizeof(struct mol_vector3));

            // Move both systems the same way
            vec_trans(&system1->coords[ligand_start], ligand1_size,
                      &center, -1.0);
            vec_trans(&system2->coords[ligand_start], ligand2_size,
                      &center, -1.0);
            mol_vector3_move_list(&system1->coords[ligand_start], ligand1_size,
                                  rotation, &translation);
            mol_vector3_move_list(&system2->coords[ligand_start], ligand2_size,
                                  rotation, &translation);
            vec_trans(&system1->coords[ligand_start], ligand1_size,
                      &center, 1.0);
            vec_trans(&system2->coords[ligand_start], ligand2_size,
                      &center, 1.0);

            E1_new = 0.0;
            vdweng(system1, &E1_new, system1_ags.nblst);
            eleng(system1, 1.0, &E1_new, system1_ags.nblst);

            E2_new = 0.0;
            vdweng(system2, &E2_new, system2_ags.nblst);
            eleng(system2, 1.0, &E2_new, system2_ags.nblst);

            E_new = lambda*E1_new + one_minus_lambda*E2_new;
            delta_E = E_new - E_cur;

            if (gsl_rng_uniform(rng) < exp(negative_beta*delta_E)) {
                E_cur = E_new;
                E1_cur = E1_new;
                E2_cur = E2_new;
                center_ligand(&center, &system1->coords[ligand_start], ligand1_size);
                naccept++;
            } else {
                // Undo transform
                memcpy(&system1->coords[ligand_start], ligand1_save,
                       ligand1_size*sizeof(struct mol_vector3));
                memcpy(&system2->coords[ligand_start], ligand2_save,
                       ligand2_size*sizeof(struct mol_vector3));
            }

            cummulative_E += E_cur;
            cummulative_Ediff += (E1_cur - E2_cur);
        }

        // Save some trajectory info
        lig_center = system1->coords[ligand_start];
        for (size_t i = ligand_start+1; i < system1->natoms; i++) {
            MOL_VEC_ADD(lig_center, lig_center, system1->coords[i]);
        }
        MOL_VEC_DIV_SCALAR(lig_center, lig_center, ligand1_size);
        fprintf(stderr, "%lf %lf %lf\n",
                lig_center.X, lig_center.Y, lig_center.Z);
    }

    double average_E = cummulative_E / (nsteps * nrounds);
    double average_Ediff = cummulative_Ediff / (nsteps * nrounds);
    printf("average_E: %lf\n", average_E);
    printf("average_Ediff: %lf\n", average_Ediff);
    printf("naccept: %ld\n", naccept);

    return 0;
}
