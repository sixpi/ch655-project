# Try to find libmol2 automatically

if(LIBMOL2_LIBRARIES AND LIBMOL2_INCLUDE_DIRS)
  set(LIBMOL2_FOUND TRUE)
else(LIBMOL2_LIBRARIES AND LIBMOL2_INCLUDE_DIRS)
  find_path(LIBMOL2_INCLUDE_DIR
    NAMES
      mol2
    PATHS
      /usr/include
      /usr/local/include
      /opt/local/include
      /sw/include
      $ENV{HOME}/include
      $ENV{HOME}/usr/include)
  find_library(LIBMOL2_LIBRARY
    NAMES
      mol2
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
      $ENV{HOME}/lib
      $ENV{HOME}/usr/lib)

  set(LIBMOL2_INCLUDE_DIRS ${LIBMOL2_INCLUDE_DIR})

  if(LIBMOL2_LIBRARY)
    set(LIBMOL2_LIBRARIES
      ${LIBMOL2_LIBRARIES}
      ${LIBMOL2_LIBRARY})
  endif(LIBMOL2_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(libmol2 DEFAULT_MSG
    LIBMOL2_LIBRARIES LIBMOL2_INCLUDE_DIRS)
endif(LIBMOL2_LIBRARIES AND LIBMOL2_INCLUDE_DIRS)

mark_as_advanced( LIBMOL2_INCLUDE_DIRS LIBMOL2_LIBRARIES )