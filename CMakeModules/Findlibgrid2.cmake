# Try to find libgrid2 automatically

if(LIBGRID2_LIBRARIES AND LIBGRID2_INCLUDE_DIRS)
  set(LIBGRID2_FOUND TRUE)
else(LIBGRID2_LIBRARIES AND LIBGRID2_INCLUDE_DIRS)
  find_path(LIBGRID2_INCLUDE_DIR
    NAMES
      grid2
    PATHS
      /usr/include
      /usr/local/include
      /opt/local/include
      /sw/include
      $ENV{HOME}/include
      $ENV{HOME}/usr/include)
  find_library(LIBGRID2_LIBRARY
    NAMES
      grid2
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
      $ENV{HOME}/lib
      $ENV{HOME}/usr/lib)

  set(LIBGRID2_INCLUDE_DIRS ${LIBGRID2_INCLUDE_DIR})

  if(LIBGRID2_LIBRARY)
    set(LIBGRID2_LIBRARIES
      ${LIBGRID2_LIBRARIES}
      ${LIBGRID2_LIBRARY})
  endif(LIBGRID2_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(libgrid2 DEFAULT_MSG
    LIBGRID2_LIBRARIES LIBGRID2_INCLUDE_DIRS)
endif(LIBGRID2_LIBRARIES AND LIBGRID2_INCLUDE_DIRS)

mark_as_advanced( LIBGRID2_INCLUDE_DIRS LIBGRID2_LIBRARIES )
